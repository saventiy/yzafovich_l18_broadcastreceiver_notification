package com.example.l18_saventiy.util

import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.l18_saventiy.R

class NotificationUtil(val context: Context) {

    fun createSampleDataNotification(text: String) {
        val channelId = "chanel_id"
        val notificationID = 911

        val notification = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("Notification")
            .setContentText(text)
            .setTimeoutAfter(200)
            .setPriority(NotificationCompat.PRIORITY_HIGH)

        NotificationManagerCompat.from(context).notify(notificationID, notification.build())
    }

//
//
//    fun createNotificationChannel() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            val name = "Hello"
//            val description = "description"
//            val importance = NotificationManager.IMPORTANCE_HIGH
//            val channel = NotificationChannel("chanel_id", name, importance)
//            channel.description = description
//            channel.enableLights(true)
//            val notificationManager = getSystemService(NotificationManager::class.java)
//            notificationManager?.createNotificationChannel(channel)
//        }
//    }
}