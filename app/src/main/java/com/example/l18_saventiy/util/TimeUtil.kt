package com.example.l18_saventiy.util

import java.text.SimpleDateFormat
import java.util.*

class TimeUtil {
    fun currentTime() = SimpleDateFormat("HH:mm").format(Calendar.getInstance().time)
}