package com.example.l18_saventiy

import android.content.Context
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import com.example.l18_saventiy.adapter.Adapter
import com.example.l18_saventiy.data.Item
import com.example.l18_saventiy.util.NotificationUtil
import com.example.l18_saventiy.util.TimeUtil


class IncommingCall(context: Context, adapter: Adapter) {

    init {
        val telManager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        val phoneListener = MyPhoneStateListener(context, adapter)

        telManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE)
    }


    class MyPhoneStateListener(val context: Context, val adapter: Adapter) : PhoneStateListener() {

        override fun onCallStateChanged(state: Int, phoneNumber: String) {
            super.onCallStateChanged(state, phoneNumber)

            if (state == TelephonyManager.CALL_STATE_RINGING) {
                adapter.addItem(Item(TimeUtil().currentTime(), phoneNumber))
                adapter.notifyDataSetChanged()
                NotificationUtil(context).createSampleDataNotification(phoneNumber)
            }
        }
    }
}