package com.example.l18_saventiy.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.l18_saventiy.data.Item
import com.example.l18_saventiy.R
import kotlinx.android.synthetic.main.item_fragment.view.*

class Adapter : RecyclerView.Adapter<Adapter.ViewHolder>() {

    private var usersList: MutableList<Item> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            from(parent)
        )
    }

    override fun getItemCount(): Int = usersList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.time.text = usersList[position].time
        holder.data.text = usersList[position].info
    }

    fun addItem(item: Item) {
        usersList.add(item)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val time = itemView.text_view_time
        val data = itemView.text_view_data
    }

    companion object {
        fun from(parent: ViewGroup) =
            LayoutInflater.from(parent.context).inflate(R.layout.item_fragment, parent, false)
    }
}
