package com.example.l18_saventiy

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.l18_saventiy.adapter.Adapter
import com.example.l18_saventiy.data.Item
import com.example.l18_saventiy.util.NotificationUtil
import com.example.l18_saventiy.util.TimeUtil

class UsbConnectionsReceiver(val context: Context?, val adapter: Adapter) : BroadcastReceiver() {


    override fun onReceive(context: Context?, intent: Intent?) {
        val action = intent?.action

        val notificationUtil =
            NotificationUtil(context!!)

        if (action != null) {
            when (action) {
                Intent.ACTION_POWER_CONNECTED -> {
                    adapter.addItem(
                        Item(
                            TimeUtil().currentTime(),
                            "Connected"
                        )
                    )
                    adapter.notifyDataSetChanged()
                    notificationUtil.createSampleDataNotification("Connected")
                }

                Intent.ACTION_POWER_DISCONNECTED -> {
                    Item(
                        TimeUtil().currentTime(),
                        "Disconnected"
                    )
                    adapter.notifyDataSetChanged()
                    notificationUtil.createSampleDataNotification("Disconnected")
                }
            }
        }
    }
}

