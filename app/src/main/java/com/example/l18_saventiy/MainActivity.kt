package com.example.l18_saventiy

import android.Manifest.permission.READ_CALL_LOG
import android.Manifest.permission.READ_PHONE_STATE
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.l18_saventiy.adapter.Adapter
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val phoneStateReceiver by lazy {   UsbConnectionsReceiver(baseContext, adapter)}
    private lateinit var adapter: Adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = Adapter()
        recycler_view_data.layoutManager = LinearLayoutManager(this)
        recycler_view_data.adapter = adapter


        if (ContextCompat.checkSelfPermission(
                this,
                READ_CALL_LOG
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this, arrayOf(READ_CALL_LOG, READ_PHONE_STATE), 0)
        }
    }

    override fun onStart() {
        super.onStart()

        setBroadcastReveiver()
        createNotificationChannel()
    }

    override fun onResume() {
        super.onResume()
        IncommingCall(baseContext, adapter)
    }

    override fun onDestroy() {
        unregisterReceiver(phoneStateReceiver)
        super.onDestroy()
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Hello"
            val description = "description"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel("chanel_id", name, importance)
            channel.description = description
            channel.enableLights(true)
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager?.createNotificationChannel(channel)
        }
    }

    private fun setBroadcastReveiver() {
        var intentFilter = IntentFilter()
        intentFilter.addAction(Intent.ACTION_POWER_CONNECTED)
        intentFilter.addAction(Intent.ACTION_POWER_DISCONNECTED)
        registerReceiver(phoneStateReceiver, intentFilter)
    }
}
